import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flexDirection: 'column',
  },
  containerFullSize: {
    width: '100%',
    height: '100%',
  },
  box: {
    width: 70,
    height: 70,
  },
  boxContent: {
    width: 'auto',
    minWidth: 70,
  },
});
class Flexing extends Component {
  render() {
    return (
      <View style={[styles.containerFullSize, styles.container]}>
        <View style={{alignItems: 'center'}}>
          <Text>FLEX</Text>
        </View>
        <View style={{flex: 1, backgroundColor: 'red'}}>
          <Text>Ini Header</Text>
        </View>
        <View style={{flex: 3, backgroundColor: 'darkorange'}}>
          <Text>Ini Content</Text>
        </View>
        <View style={{flex: 1, backgroundColor: 'green'}}>
          <Text>Ini Footer</Text>
        </View>
      </View>
    );
  }
}
class JustifyContent extends Component {
  render() {
    return (
      <View>
        <View
          style={[
            styles.containerFullSize,
            {
              justifyContent: 'flex-end',
            },
          ]}>
          <View style={[styles.box, {backgroundColor: 'powderblue'}]}></View>
          <View style={[styles.box, {backgroundColor: 'skyblue'}]}></View>
          <View style={[styles.box, {backgroundColor: 'steelblue'}]}></View>
        </View>
      </View>
    );
  }
}
class AlignItems extends Component {
  render() {
    return (
      <View>
        <View
          style={[
            styles.containerFullSize,
            {
              alignItems: 'baseline',
            },
          ]}>
          <View style={[styles.box, {backgroundColor: 'powderblue'}]}></View>
          <View style={[styles.box, {backgroundColor: 'skyblue'}]}></View>
          <View
            style={[
              styles.box,
              {backgroundColor: 'steelblue', width: 'auto', minWidth: 70},
            ]}></View>
        </View>
      </View>
    );
  }
}
class AlignSelf extends Component {
  render() {
    return (
      <View>
        <View
          style={[
            styles.containerFullSize,
            {
              alignItems: 'baseline',
            },
          ]}>
          <View style={[styles.box, {backgroundColor: 'powderblue'}]}></View>
          <View style={[styles.box, {backgroundColor: 'skyblue'}]}></View>
          <View
            style={[
              styles.box,
              {
                backgroundColor: 'steelblue',
                width: 'auto',
                minWidth: 70,
                alignSelf: 'baseline',
              },
            ]}></View>
        </View>
      </View>
    );
  }
}
class AlignContent extends Component {
  render() {
    return (
      <View>
        <View
          style={[
            styles.containerFullSize,
            {
              flexDirection: 'column',
              flexWrap: 'wrap',
              alignContent: 'flex-start',
            },
          ]}>
          <View
            style={[
              styles.box,
              styles.boxContent,
              {
                backgroundColor: 'powderblue',
                height: '33%',
              },
            ]}></View>
          <View
            style={[
              styles.box,
              styles.boxContent,
              {
                backgroundColor: 'skyblue',
                height: '33%',
              },
            ]}></View>
          <View
            style={[
              styles.box,
              styles.boxContent,
              {
                backgroundColor: 'steelblue',
                height: '34%',
              },
            ]}></View>
          <View
            style={[
              styles.box,
              styles.boxContent,
              {backgroundColor: 'lightgreen'},
            ]}></View>
          <View style={[styles.box, {backgroundColor: 'green'}]}></View>
          <View
            style={[
              styles.box,
              styles.boxContent,
              {backgroundColor: 'darkgreen'},
            ]}></View>
        </View>
      </View>
    );
  }
}
class Positioning extends Component {
  render() {
    return (
      <View style={{flexDirection: 'row'}}>
        <View style={[styles.containerFullSize, {flex: 1}]}>
          <View
            style={[
              styles.box,
              {
                backgroundColor: 'powderblue',
                top: 20,
                left: 20,
                position: 'relative',
              },
            ]}></View>
          <View
            style={[
              styles.box,
              {
                backgroundColor: 'skyblue',
                top: 50,
                left: 50,
                position: 'relative',
              },
            ]}></View>
          <View
            style={[
              styles.box,
              {
                backgroundColor: 'steelblue',
                top: 80,
                left: 80,
                position: 'relative',
              },
            ]}></View>
        </View>
        <View style={[styles.containerFullSize, {flex: 1}]}>
          <View
            style={[
              styles.box,
              {
                backgroundColor: 'powderblue',
                top: 20,
                left: 20,
                position: 'absolute',
              },
            ]}></View>
          <View
            style={[
              styles.box,
              {
                backgroundColor: 'skyblue',
                top: 50,
                left: 50,
                position: 'absolute',
              },
            ]}></View>
          <View
            style={[
              styles.box,
              {
                backgroundColor: 'steelblue',
                top: 80,
                left: 80,
                position: 'absolute',
              },
            ]}></View>
        </View>
      </View>
    );
  }
}
export default class Layouting extends Component {
  render() {
    return <Flexing />;
  }
}
