import React, {Component} from 'react';
import {View, Text, StyleSheet, Pressable, Dimensions} from 'react-native';
const styles = StyleSheet.create({
  container: {
    padding: 16,
    height: '100%',
  },
  header: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'darkblue',
  },
  headerTitle: {
    fontSize: 18,
    textTransform: 'uppercase',
    backgroundColor: 'yellow',
    padding: 5,
  },
  paragraf: {
    margin: 10,
    padding: 10,
    fontSize: 12,
    borderColor: 'teal',
    borderWidth: 1,
    backgroundColor: 'white',
    width: '100%',
  },
  btn: {
    padding: 15,
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
  },
  btnLabel: {
    textTransform: 'uppercase',
    fontSize: 14,
  },
});
export class ButtonKita extends Component {
  render() {
    return (
      <Pressable
        onPress={() => {
          this.props.onClick && this.props.onClick();
        }}
        style={[
          styles.btn,
          {
            backgroundColor:
              this.props.backgroundColor ?? styles.btn.backgroundColor,
          },
        ]}>
        <Text
          style={[
            styles.btnLabel,
            {color: this.props.labelColor ?? styles.btn.color},
          ]}>
          {this.props.label}
        </Text>
      </Pressable>
    );
  }
}
export default class Styling extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            {alignItems: 'center', backgroundColor: '#dadada'},
            styles.container,
          ]}>
          <Text style={styles.header}>WORKSHOP RN BEGINNER</Text>
          <Text style={[styles.header, styles.headerTitle]}>react native</Text>
          <Text style={styles.paragraf}>Kampus Merdeka</Text>
          <ButtonKita
            label="Button Kita"
            backgroundColor="red"
            labelColor="white"
            onClick={() => alert('button disentuh!')}
          />
        </View>
      </View>
    );
  }
}
