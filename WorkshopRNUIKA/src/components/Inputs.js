import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Switch,
  Image,
  Button,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import CheckBox from '@react-native-community/checkbox';
import RadioGroup from 'react-native-radio-buttons-group';
import {launchCamera} from 'react-native-image-picker';
import DatePicker from 'react-native-modern-datepicker';
const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  inputText: {
    width: '100%',
    borderColor: 'teal',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginVertical: 5,
  },
  title: {alignSelf: 'center', fontWeight: 'bold', fontSize: 18},
  logBox: {
    marginTop: 10,
    borderColor: 'black',
    borderWidth: 0.5,
    padding: 10,
  },
  listPicker: {
    width: '100%',
    padding: 10,
    marginVertical: 5,
  },
});
class InputText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input1: null,
      input2: null,
      input3: null,
      input4: null,
      input5: null,
    };
    this.onChangeInput = this.onChangeInput.bind(this);
  }
  onChangeInput = (stateKey, value) => this.setState({[stateKey]: value});
  render() {
    return (
      <View>
        <Text style={styles.title}>Input</Text>
        <TextInput
          style={[styles.inputText]}
          keyboardType="default"
          placeholder="Input Text"
          key="input1"
          value={this.state.input1}
          onChangeText={val => this.onChangeInput('input1', val)}
        />
        <TextInput
          style={[styles.inputText]}
          keyboardType="email-address"
          placeholder="Input Email"
          key="input2"
          value={this.state.input2}
          onChangeText={val => this.onChangeInput('input2', val)}
        />
        <TextInput
          style={[styles.inputText]}
          keyboardType="phone-pad"
          placeholder="Input Phone"
          key="input3"
          value={this.state.input3}
          onChangeText={val => this.onChangeInput('input3', val)}
        />
        <TextInput
          style={[styles.inputText]}
          keyboardType="default"
          placeholder="Input Password"
          secureTextEntry={true}
          key="input4"
          value={this.state.input4}
          onChangeText={val => this.onChangeInput('input4', val)}
        />

        <TextInput
          style={[styles.inputText]}
          keyboardType="default"
          placeholder="Input Address"
          multiline
          numberOfLines={3}
          key="input5"
          value={this.state.input5}
          onChangeText={val => this.onChangeInput('input5', val)}
        />

        <View style={styles.logBox}>
          <Text style={styles.title}>Log Input Text</Text>
          <Text>
            {Object.keys(this.state).map(key => {
              return `${key}:${this.state[key]}\n`;
            })}
          </Text>
        </View>
      </View>
    );
  }
}
// Dependencies
// @react-native-picker/picker
// react-native-modern-datepicker
class InputPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option1: [
        {value: null, label: 'Pilih Jenis Kelamin'},
        {value: 'L', label: 'Laki-laki'},
        {value: 'P', label: 'Perempuan'},
      ],
      option1SelectedValue: null,
      date: '2021-08-22',
      time: '00:00',
    };
    this.onValueChange = this.onValueChange.bind(this);
  }

  onValueChange = (key, val, index) => {
    this.setState({[key]: val});
  };

  render() {
    return (
      <View style={this.props.style}>
        <Text style={styles.title}>Input Picker</Text>
        <Picker
          style={[styles.listPicker]}
          key="option1"
          selectedValue={this.state.option1SelectedValue}
          onValueChange={(val, index) =>
            this.onValueChange('option1SelectedValue', val, index)
          }>
          {this.state.option1.map((item, i) => (
            <Picker.Item
              key={`option1-item-${i}`}
              label={item.label}
              value={item.value}
            />
          ))}
        </Picker>
        <DatePicker
          mode="time"
          minuteInterval={3}
          onTimeChange={selectedTime => this.setState({time: selectedTime})}
        />
        <DatePicker
          mode="datepicker"
          onDateChange={selectedDate => this.setState({date: selectedDate})}
        />
        <View style={styles.logBox}>
          <Text style={styles.title}>Input Picker Log</Text>
          <Text>
            {Object.keys(this.state).map(key => {
              return `${key}:${this.state[key]}\n`;
            })}
          </Text>
        </View>
      </View>
    );
  }
}

//Dependencies
//react-native-radio-buttons-group
//@react-native-community/checkbox
class CheckBoxRadioSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      swActive: false,
      cbData: [
        {
          id: 1,
          name: 'Daging Ikan',
          value: false,
        },
        {
          id: 2,
          name: 'Daging Sapi',
          value: false,
        },
        {
          id: 3,
          name: 'Daging Kambing',
          value: false,
        },
        {
          id: 4,
          name: 'Daging Kelalawar',
          value: false,
        },
      ],
      rbData: [
        {
          id: '1',
          label: 'Suka',
          value: 'suka',
        },
        {
          id: '2',
          label: 'Tidak Suka',
          value: 'tidak suka',
        },
      ],
    };
  }

  render() {
    return (
      <View style={this.props.style}>
        <Text style={styles.title}>Switch,CheckBox,Radio</Text>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <Switch
            key="sw1"
            trackColor={{false: '#767577', true: '#81b0ff'}}
            thumbColor={this.state.swActive ? '#f5dd4b' : '#f4f3f4'}
            ios_backgroundColor="#3e3e3e"
            value={this.state.swActive}
            onValueChange={val =>
              this.setState({swActive: !this.state.swActive})
            }
          />
          <Text style={{flex: 1}}>
            {this.state.swActive ? 'Aktif' : 'Tidak Aktif'}
          </Text>
        </View>
        <View>
          {this.state.cbData.map(cb => (
            <View key={`cbView-${cb.id}`} style={{flexDirection: 'row'}}>
              <CheckBox
                key={`cb-${cb.id}`}
                value={cb.value}
                onValueChange={val => {
                  let cbData = this.state.cbData;
                  cbData.map(item => {
                    if (item.id === cb.id) {
                      item.value = val;
                    }
                    return item;
                  });
                  this.setState({cbData});
                }}
              />
              <Text key={`cbText-${cb.id}`} style={{flex: 1}}>
                {cb.name}
              </Text>
            </View>
          ))}
        </View>
        <View style={{justifyContent: 'flex-start'}}>
          <RadioGroup
            containerStyle={{alignItems: 'baseline'}}
            radioButtons={this.state.rbData}
            onPress={val => this.setState({rbData: val})}
          />
        </View>
        <View style={styles.logBox}>
          <Text style={styles.title}>Log Switch,CheckBox,Radio</Text>
          <Text>
            {Object.keys(this.state).map(key => {
              return `${key}:${JSON.stringify(this.state[key])}\n`;
            })}
          </Text>
        </View>
      </View>
    );
  }
}

//Dependencies
//react-native-image-picker
class InputImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
    };
    this.imageChooser = this.imageChooser.bind(this);
  }
  imageChooser = () => {
    try {
      launchCamera(
        {
          mediaType: 'photo',
        },
        res => {
          this.setState({
            image:
              res.assets != null && res.assets.length > 0
                ? res.assets[0]
                : null,
          });
        },
      );
    } catch (err) {
      this.setState({image: null});
    }
  };
  render() {
    return (
      <View style={this.props.style}>
        <Text style={styles.title}>Pilih Gambar</Text>
        <Button title="Pilih Gambar" color="teal" onPress={this.imageChooser} />
        {this.state.image && (
          <Image
            source={{uri: this.state.image.uri}}
            style={{width: '100%', height: 250}}
            resizeMode="contain"
          />
        )}
        <View style={styles.logBox}>
          <Text style={styles.title}>Log Image Chooser</Text>
          <Text>
            {Object.keys(this.state).map(key => {
              return `${key}:${JSON.stringify(this.state[key])}\n`;
            })}
          </Text>
        </View>
      </View>
    );
  }
}
export default class Inputs extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <InputText />
          <InputPicker
            style={{marginTop: 20, borderTopColor: 'black', borderTopWidth: 1}}
          />
          <CheckBoxRadioSwitch
            style={{marginTop: 20, borderTopColor: 'black', borderTopWidth: 1}}
          />
          <InputImage
            style={{marginTop: 20, borderTopColor: 'black', borderTopWidth: 1}}
          />
        </View>
      </ScrollView>
    );
  }
}
