import {Picker} from '@react-native-picker/picker';
import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Button,
  StyleSheet,
  Dimensions,
  TextInput,
  Pressable,
  ScrollView,
} from 'react-native';
import {launchCamera} from 'react-native-image-picker';
import DatePicker from 'react-native-modern-datepicker';

const styles = StyleSheet.create({
  container: {
    padding: 16,
    fontSize: 12,
  },
  imgBackground: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    top: 0,
    left: 0,
    right: 0,
  },
  card: {
    backgroundColor: 'rgba(255,255,255,0.95)',
    width: '100%',
    padding: 10,
    borderRadius: 15,
    elevation: 5,
  },
  cardContent: {
    padding: 10,
  },
  cardTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: 'darkorange',
    alignSelf: 'center',
  },
  inputLabel: {
    fontSize: 11,
  },
  inputControl: {
    borderBottomColor: 'darkorange',
    borderBottomWidth: 1,
    width: '100%',
    padding: 3,
    marginBottom: 5,
    fontSize: 16,
  },
  iconImg: {
    height: 25,
    width: 25,
  },
  row: {
    flexDirection: 'row',
  },
});
const genderData = [
  {
    value: null,
    label: 'Pilih Jenis Kelamin',
  },
  {
    value: 'Laki-laki',
    label: 'Laki-laki',
  },
  {
    value: 'Perempuan',
    label: 'Perempuan',
  },
];
const initState = {
  username: null,
  password: null,
  pwdSecure: true,
  pwdIconImg: 'https://image.flaticon.com/icons/png/512/65/65000.png',
  tglLahir: null,
  gender: null,
  selfiePhoto: null,
  showTglLahirPicker: false,
  showData: false,
};
export default class FormQuiz extends Component {
  constructor(props) {
    super(props);
    this.state = initState;
    this.onSubmit = this.onSubmit.bind(this);
    this.imageChooser = this.imageChooser.bind(this);
  }
  onSubmit = () => this.setState({showData: true});
  imageChooser = () => {
    try {
      launchCamera(
        {
          mediaType: 'photo',
        },
        res => {
          this.setState({
            selfiePhoto:
              res.assets != null && res.assets.length > 0
                ? res.assets[0]
                : null,
          });
        },
      );
    } catch (err) {
      this.setState({selfiePhoto: null});
    }
  };

  render() {
    return (
      <ScrollView>
        <View
          style={[
            styles.container,
            {minHeight: Dimensions.get('window').height},
          ]}>
          <Image
            style={[styles.imgBackground]}
            resizeMode="cover"
            source={{
              uri: 'https://png.pngtree.com/thumb_back/fw800/back_our/20190620/ourmid/pngtree-mobile-banking-app-welcome-page-startup-page-h5-background-psd-image_153932.jpg',
            }}
          />
          {this.state.showData ? (
            <View style={[styles.card]}>
              <Text style={[styles.cardTitle]}>Data Registrasi</Text>
              <View style={[styles.cardContent]}>
                <Text>{JSON.stringify(this.state)}</Text>
                <Button
                  title="Kembali"
                  onPress={() => this.setState(initState)}></Button>
              </View>
            </View>
          ) : (
            <View style={[styles.card]}>
              <Text style={[styles.cardTitle]}>Form Registrasi</Text>
              <View style={[styles.cardContent]}>
                <View>
                  <Text style={[styles.inputLabel]}>Username</Text>
                  <TextInput
                    keyboardType="default"
                    value={this.state.username}
                    onChangeText={val => this.setState({username: val})}
                    style={[styles.inputControl]}
                    placeholder="Type here..."
                  />
                </View>
                <View>
                  <Text style={[styles.inputLabel]}>Password</Text>
                  <View style={[styles.row]}>
                    <TextInput
                      keyboardType="default"
                      value={this.state.password}
                      onChangeText={val => this.setState({password: val})}
                      style={[styles.inputControl, {flex: 1}]}
                      secureTextEntry={this.state.pwdSecure}
                      placeholder="Type here..."
                    />
                    <Pressable
                      onPress={() => {
                        const imgVisible =
                          'https://image.flaticon.com/icons/png/512/65/65000.png';
                        const imgInvisible =
                          'https://icon-library.com/images/show-password-icon/show-password-icon-18.jpg';
                        if (this.state.pwdSecure) {
                          this.setState({
                            pwdSecure: !this.state.pwdSecure,
                            pwdIconImg: imgInvisible,
                          });
                        } else {
                          this.setState({
                            pwdSecure: !this.state.pwdSecure,
                            pwdIconImg: imgVisible,
                          });
                        }
                      }}>
                      <Image
                        style={[styles.iconImg]}
                        source={{
                          uri: this.state.pwdIconImg,
                        }}
                      />
                    </Pressable>
                  </View>
                </View>
                <View>
                  <Text style={[styles.inputLabel]}>Jenis Kelamin</Text>
                  <Picker
                    style={[styles.inputControl]}
                    selectedValue={this.state.gender}
                    onValueChange={val => {
                      this.setState({gender: val});
                    }}>
                    {genderData.map((item, i) => (
                      <Picker.Item
                        key={`gender-item-${i}`}
                        label={item.label}
                        value={item.value}
                      />
                    ))}
                  </Picker>
                </View>
                <View>
                  <Text style={[styles.inputLabel]}>Tanggal Lahir</Text>
                  {this.state.showTglLahirPicker ? (
                    <DatePicker
                      mode="datepicker"
                      onDateChange={selectedDate =>
                        this.setState({
                          tglLahir: selectedDate,
                          showTglLahirPicker: false,
                        })
                      }
                    />
                  ) : (
                    <Pressable
                      style={[styles.inputControl]}
                      onPress={() => this.setState({showTglLahirPicker: true})}>
                      <Text>{this.state.tglLahir ?? 'Pilih Tanggal'}</Text>
                    </Pressable>
                  )}
                </View>

                <View>
                  <Text style={[styles.inputLabel]}>Foto Selfie</Text>
                  <Pressable onPress={this.imageChooser}>
                    <Image
                      source={{
                        uri:
                          this.state.selfiePhoto == null
                            ? 'https://image.flaticon.com/icons/png/512/55/55999.png'
                            : this.state.selfiePhoto.uri,
                      }}
                      style={{
                        width: '100%',
                        height: this.state.selfiePhoto == null ? 150 : 250,
                      }}
                      resizeMode="contain"
                    />
                  </Pressable>
                </View>
                <View style={{marginVertical: 10}}>
                  <Button
                    title="Submit"
                    color="darkorange"
                    onPress={this.onSubmit}
                  />
                </View>
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    );
  }
}
