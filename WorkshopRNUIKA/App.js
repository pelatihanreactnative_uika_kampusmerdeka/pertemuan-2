import React, {Component} from 'react';
import {SafeAreaView, View} from 'react-native';
import Inputs from './src/components/Inputs';
import Layouting from './src/components/Layouting';
import Styling from './src/components/Styling';
import FormQuiz from './src/screens/FormQuiz';

class App extends Component {
  render() {
    return (
      <SafeAreaView>
        <View>
          <FormQuiz />
        </View>
      </SafeAreaView>
    );
  }
}

export default App;
